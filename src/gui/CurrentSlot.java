package gui;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

public class CurrentSlot extends Observable {

	private ArrayList<SlotLabel> labelList;
	private SlotLabel currentSlot;
	private int columns;
	private String SlotNumber;

	/**
	 * Denna klassen ska h�lla reda p� vilken cell som �r
	 * vald just nu. Denna klassen ska skicka information
	 * till vyn om t.ex. vilken cell som �r vald just nu
	 * och uppdatera. Vyn observerar denna klassen.
	 */

	public CurrentSlot(List<SlotLabel> labelList, int columns) {
		super();
		this.labelList = (ArrayList<SlotLabel>)labelList;
		this.columns = columns;
		SlotNumber = "A1";
		setChanged();
	}

	public void init() {
		currentSlot = this.labelList.get(0);
		currentSlot.setBackground(Color.YELLOW);
	}
	
	public String getAddress() {
		return SlotNumber;
	}

	public void setCurrentSlot(SlotLabel currentSlot) {
		this.currentSlot.setBackground(Color.WHITE);
		this.currentSlot = currentSlot;
		currentSlot.setBackground(Color.YELLOW);

		int temp = 0;
		while(currentSlot != labelList.get(temp)) {
			temp++;

		}
		SlotNumber = "" + Character.toString((char)('A' + temp % columns)) + (temp / columns + 1);
		setChanged();
		notifyObservers();
	}
}
