package gui;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class LabelListener implements MouseListener {
	private CurrentSlot current;
	private SlotLabel listenedLabel;
	
	public LabelListener(CurrentSlot current, SlotLabel listenedLabel) {
		this.current = current;
		this.listenedLabel = listenedLabel;
	}

	public void mousePressed(MouseEvent e) {
		current.setCurrentSlot(listenedLabel);
	}
	
	// Not applicable to our program
	public void mouseClicked(MouseEvent e) {}
	public void mouseReleased(MouseEvent e) {}
	public void mouseEntered(MouseEvent e) {}
	public void mouseExited(MouseEvent e) {}
}
