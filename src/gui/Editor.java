package gui;

import java.awt.Color;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JTextField;

import backend.CircularDependencyException;
import backend.Spreadsheet;

@SuppressWarnings("serial")
public class Editor extends JTextField implements Observer {
	private Spreadsheet sheet;
	private String address;

	public Editor(Spreadsheet sheet) {
		setBackground(Color.WHITE);
		addKeyListener(new EnterListener());
	}

	private class EnterListener implements KeyListener {
		public void keyTyped(KeyEvent e) {}

		public void keyPressed(KeyEvent e) {
			if(e.getKeyCode() == KeyEvent.VK_ENTER) {
				put();
			}
		}

		public void keyReleased(KeyEvent e) {}
	}

	public void put() {
		try {
			sheet.put(address, getText());
		} catch (CircularDependencyException e) {
			sheet.put(address, "");
			sheet.updater("Circular dependency found");
		}
	}

	public void update(Observable o, Object arg) {
		if(o instanceof Spreadsheet) {
			sheet = (Spreadsheet)o;
		}
		if(o instanceof CurrentSlot) {
			try {
				address = ((CurrentSlot) o).getAddress();
				this.setText(sheet.get(address).toString());
			} catch(Exception e) {
				this.setText("");
			}
		}
	}
}