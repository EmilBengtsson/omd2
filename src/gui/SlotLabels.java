package gui;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import javax.swing.SwingConstants;

import backend.Spreadsheet;

@SuppressWarnings("serial")
public class SlotLabels extends GridPanel {
    private List<SlotLabel> labelList;
    private CurrentSlot currentSlot;

    public SlotLabels(int rows, int cols) {
        super(rows + 1, cols);
        labelList = new ArrayList<SlotLabel>(rows * cols);
        currentSlot = new CurrentSlot(labelList, cols);
        for (char ch = 'A'; ch < 'A' + cols; ch++) {
            add(new ColoredLabel(Character.toString(ch), Color.LIGHT_GRAY,
                    SwingConstants.CENTER));
        }
        for (int row = 1; row <= rows; row++) {
            for (char ch = 'A'; ch < 'A' + cols; ch++) {
                SlotLabel label = new SlotLabel(("" + ch + row));
                label.addMouseListener(new LabelListener(currentSlot, label));
                add(label);
                labelList.add(label);
            }
        }
        
        currentSlot.init();
    }
    
    public void addCurrentSlotObservers(CurrentLabel cl, Editor edit) {
    	currentSlot.addObserver(cl);
    	currentSlot.addObserver(edit);
    	currentSlot.notifyObservers();
    }
    
    public void addSheetObservers(Spreadsheet sheet) {
    	for(SlotLabel sl : labelList) {
    		sheet.addObserver(sl);
    	}
    }
}