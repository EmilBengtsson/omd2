package gui.menu;

import gui.StatusLabel;
import gui.XL;
import util.XLException;
import java.io.FileNotFoundException;
import java.io.IOException;
import javax.swing.JFileChooser;
import backend.Spreadsheet;
import backend.XLBufferedReader;

@SuppressWarnings("serial")
class LoadMenuItem extends OpenMenuItem {

	public LoadMenuItem(XL xl, StatusLabel statusLabel) {
		super(xl, statusLabel, "Load");
	}

	protected void action(String path) {
		XLBufferedReader reader;
		try {
			reader = new XLBufferedReader(path);
			Spreadsheet sheet = xl.getSheet();
			sheet.clearAll();
			try {
				if (reader.load(xl.getSheet())) {
					sheet.updater(path + " loaded successfully");
				} else {
					sheet.updater(path + " loaded with errors");
				}
				
			} catch (XLException e) {
				sheet.updater(e.getMessage());
				
			}
			try {
				reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			statusLabel.setText("File not found");
		}
	}

	protected int openDialog(JFileChooser fileChooser) {
		return fileChooser.showOpenDialog(xl);
	}
}