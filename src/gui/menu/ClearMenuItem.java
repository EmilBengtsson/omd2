package gui.menu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JMenuItem;

import gui.CurrentLabel;
import gui.XL;

@SuppressWarnings("serial")
class ClearMenuItem extends JMenuItem implements ActionListener {
	private XL xl;
	private CurrentLabel currentLabel;
	
	public ClearMenuItem(XL xl, CurrentLabel currentLabel) {
        super("Clear");
        addActionListener(this);
        this.xl = xl;
        this.currentLabel = currentLabel;
    }

    public void actionPerformed(ActionEvent e) {
    	xl.getSheet().put(currentLabel.getText(), "");
    }
}