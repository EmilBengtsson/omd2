package gui.menu;

import gui.StatusLabel;
import gui.XL;
import java.io.FileNotFoundException;
import javax.swing.JFileChooser;
import backend.XLPrintStream;

@SuppressWarnings("serial")
class SaveMenuItem extends OpenMenuItem {
    public SaveMenuItem(XL xl, StatusLabel statusLabel) {
        super(xl, statusLabel, "Save");
    }

    protected void action(String path) throws FileNotFoundException {
    	XLPrintStream printer = new XLPrintStream(path);
    	printer.save(xl.getSheet());
    	statusLabel.setText(path + " saved");
    	printer.close();
    }

    protected int openDialog(JFileChooser fileChooser) {
        return fileChooser.showSaveDialog(xl);
    }
}