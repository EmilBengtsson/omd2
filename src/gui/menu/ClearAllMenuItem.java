package gui.menu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JMenuItem;
import gui.XL;

@SuppressWarnings("serial")
class ClearAllMenuItem extends JMenuItem implements ActionListener {
	private XL xl;
	
    public ClearAllMenuItem(XL xl) {
        super("Clear All");
        addActionListener(this);
        this.xl = xl;
    }

    public void actionPerformed(ActionEvent e) {
    	xl.getSheet().clearAll();
    }
}