package gui;

import java.awt.Color;
import java.util.Observable;
import java.util.Observer;

import backend.ExpressionSlot;
import backend.Slot;
import backend.Spreadsheet;

@SuppressWarnings("serial")
public class SlotLabel extends ColoredLabel implements Observer {
	private String address;

	public SlotLabel(String address) {
		super("                    ", Color.WHITE, RIGHT);
		this.address = address;
	}

	public void update(Observable o, Object arg) {
		Spreadsheet sheet = (Spreadsheet) o;
		Slot s = sheet.get(address);
		if (s == null)
			setText("");
		else if (s instanceof ExpressionSlot)
			setText("" + sheet.value(address));
		else
			setText(s.toString().substring(1));
	}
}