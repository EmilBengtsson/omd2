package gui;

import java.awt.Color;
import java.util.Observable;
import java.util.Observer;

@SuppressWarnings("serial")
public class CurrentLabel extends ColoredLabel implements Observer {
    public CurrentLabel() {
        super("A1", Color.WHITE);
    }

	@Override
	public void update(Observable o, Object arg) {
		setText(((CurrentSlot) o).getAddress());
	}
}