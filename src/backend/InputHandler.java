package backend;

import java.io.IOException;
import expr.*;

public class InputHandler {
	private String value;
	
	public InputHandler() {
		value = "";
	}
	
	public Slot load(String value) {
		this.value = value;
		if (value.length() > 0) {
			return create();
		} else {
			return null;
		}
	}
	
	private Slot create() {
		if (value.charAt(0) == '#') {
			return new CommentSlot(value);
		} else {
			try {
				new ExprParser().build(value);
				return new ExpressionSlot(value);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return null; 
	}
}
