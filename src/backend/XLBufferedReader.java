package backend;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class XLBufferedReader extends BufferedReader {
	public XLBufferedReader(String name) throws FileNotFoundException {
		super(new FileReader(name));
	}

	public boolean load(Spreadsheet sheet) {
		boolean successful = true;
		try {
			while (ready()) {
				String string = readLine();
				int i = string.indexOf('=');
				String address = string.substring(0, i);
				String value = string.substring(i + 1);
				if (!sheet.put(address, value)) {
					successful = false;
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return successful;

	}
}
