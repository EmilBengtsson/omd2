package backend;

public class NullSlot extends Slot {

	public NullSlot(String value) {
		super(value);
	}

	@Override
	public double calculate(Spreadsheet sheet) {
		throw new CircularDependencyException("Circular dependency found");
	}
	
}
