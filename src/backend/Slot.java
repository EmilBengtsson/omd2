package backend;

public abstract class Slot {
	protected String value;
	
	public Slot(String value) {
		this.value = value;
	}
	
	public abstract double calculate(Spreadsheet sheet);
	
	public String toString() {
		return value;
	}
}
