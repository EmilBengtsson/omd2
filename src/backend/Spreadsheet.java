package backend;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Observable;
import expr.Environment;
import util.XLException;

public class Spreadsheet extends Observable implements Environment {
	private Map<String, Slot> sheet;
	private InputHandler handler;

	public Spreadsheet() {
		sheet = new HashMap<>();
		handler = new InputHandler();
		setChanged();
	}

	public boolean put(String address, String value) {
		Slot s;
		try {
			s = handler.load(value);
		} catch (XLException e) {
			updater(e.getMessage());
			return false;
		}
		if (s != null) {
			Slot temp = sheet.put(address, new NullSlot(value));
			try {
				s.calculate(this);
				sheet.remove(address);
				sheet.put(address, s);
				for (Slot i : sheet.values())
					i.calculate(this);
			} catch (Exception e) {
				sheet.put(address, temp);
				updater(e.getMessage());
				return false;
			}
			updater("Put " + value + " in " + address);
			return true;
		}
		Slot temp = sheet.remove(address);
		try {
			for (Slot i : sheet.values()) {
				i.calculate(this);
			}
		} catch(Exception e) {
			sheet.put(address, temp);
			updater(e.getMessage());
			return false;
		}
		updater("Cleared slot " + address);
		return true;
	}

	public void clearAll() {
		sheet = new HashMap<>();
		updater("Cleared all slots");
	}

	public void updater(String message) {
		setChanged();
		notifyObservers(message);
	}

	public double value(String address) {
		Slot slot = get(address);
		if (slot != null) {
			return slot.calculate(this);
		} else {
			throw new XLException("This address doesn't exist");
		}
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		Iterator<Entry<String, Slot>> itr = sheet.entrySet().iterator();
		while (itr.hasNext()) {
			Entry<String, Slot> e = itr.next();
			String key = e.getKey();
			Slot value = e.getValue();

			sb.append(key).append("=");
			sb.append(value).append("\n");
		}
		return sb.toString();
	}

	public Slot get(String address) {
		return sheet.get(address);
	}
}
