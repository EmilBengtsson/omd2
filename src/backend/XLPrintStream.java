package backend;

import java.io.FileNotFoundException;
import java.io.PrintStream;

public class XLPrintStream extends PrintStream {
    public XLPrintStream(String fileName) throws FileNotFoundException {
        super(fileName);
    }

    public void save(Spreadsheet sheet) {
    	print(sheet);
        flush();
        close();
    }
}
