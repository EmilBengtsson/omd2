package backend;
import java.io.IOException;
import expr.*;
import util.XLException;

public class ExpressionSlot extends Slot {
	private ExprParser parser;

	public ExpressionSlot(String value) {
		super(value);
		parser = new ExprParser();
	}

	public double calculate(Spreadsheet sheet) {
		try {
			Expr expr = parser.build(value);
			return expr.value(sheet);
		} catch (IOException e) {
			System.out.println("This error will probably never happen.");
		} catch (UnsupportedOperationException e) {
			sheet.updater(e.getMessage());
		}
		throw new XLException("Something went wrong during parsing");
	}

}
