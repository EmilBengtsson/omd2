EDA061 – XL-frågor
1 ) Vilka klasser bör finnas för att representera ett kalkylark?
Svar: Det behövs klasser som representerar ett användargränssnitt, i detta fall paketet GUI. Det saknar all funktionalitet och måste därför samarbeta med en modell. Vi behöver en klass eller ett paket av klasser som representerar rutnätet med olika värden, t.ex. en matris med 10x10 platser. Detta ska då vara en del av modellen i programmet. Kontrollen sker i stora drag i menyerna, vilket redan är implementerade förutom de klasser som har direkt anknytning till modellen.

Vi behöver två klasser: ett "sheet" och en "cell". Sheet innehåller en matris bestående av celler.

2 ) En ruta i kalkylarket skall kunna innehålla en text eller ett uttryck. Hur modellerar man detta?
Svar: Man vill att klassen som representerar en ruta vill ta emot en typparameter som både text och uttryck delar på. Då skulle t.ex. en klass som motsvarar en kommentar och en annan klass som motsvarar ett uttryck kunna implementera samma interface eller ärva från samma klass.

I klassen "Cell" tänkte vi att vi har ett attribut av typen "CellContent" som är superklass till klasserna "Comment" och "Expression"

3 ) Hur skall man hantera uppdragsgivarens krav på minnesresurser?
Svar: Uppdragsgivarens krav är att modellen för kalkylarket inte skall bero av arkets storlek utan endast den mängd information som matats in i arket. Menar de att matrisens storlek ska anpassas efter antalet angivna värden?

Inte initialisera "blanka" rutor.

4 ) Vilka klasser skall vara observatörer och vilka skall observeras?
Svar: Användargränssnittet ska inte kunna agera själv men måste se modellen för att veta hur den ska uppdateras. Det borde därför finnas en klass som observerar modellen så att användargränssnittet vet när det ska uppdateras.

XLList observeras av ModelList, StatusLabel observerar InputHandler, SlotLabels observerar CurrentCell.

5 ) Vilket paket och vilken klass skall hålla reda på vad som är ”Current slot”?
Svar: CurrentCell.java i paketet model.

6 ) Vilken funktionalitet är redan färdig och hur fungerar den? Titta på klasserna i vy-paketet och testkör.
Svar: Print: skriver ut, close: stänger fönstret, new: skapar ett nytt fönster och lägger till det i en lista med alla fönster.

7 ) Det kan inträffa ett antal olika fel när man försöker ändra innehållet i ett kalkylark. Då skall undantag kastas. Var skall dessa undantag fångas och hanteras?
Svar: i "InputHandler"

8 ) Vilken klass används för att representera en adress i ett uttryck?
Svar: "Cell"

9 ) När ett uttryck som består av en adress skall beräknas används gränssnittet Environment. Vilken klass skall implementera gränssnittet? Varför använder man inte klassnamnet i stället för gränssnittet?
Svar: "CellList", om man vill kunna göra flera olika sorters kalkylark i framtiden

10 ) Om ett uttryck i kalkylarket refererar till sig själv, direkt eller indirekt, så kommer det att bli bekymmer vid beräkningen av uttryckets värde. Föreslå något sätt att upptäcka sådana cirkulära beroenden! Det finns en elegant lösning med hjälp av strategimönstret som du får chansen att upptäcka. Om du inte hittar den så kommer handledaren att avslöja den.
Svar: Om man sätter cellen man vill lagra informationen efter en uträkning i till null innan själva uträkningen sker kommer man att få en nullpointerexception om uttrycket refererar till sig själv